package main

import (
	"fmt"

	"gitlab.com/jasb91/lecciones-curso-golang/27-testing-benchmarking/03-examples/01/mate"
)

func main() {
	fmt.Println(mate.Sum(2, 4, 5))
	fmt.Println(mate.Sum(10, 11, 12))
	fmt.Println(mate.Sum(20, 40, 60))
}
