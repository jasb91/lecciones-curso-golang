package main

import "testing"

func TestMyAdd(t *testing.T) {
	type prueba struct {
		datos     []int
		respuesta int
	}

	pruebas := []prueba{
		prueba{[]int{2, 4, 6}, 12},
		prueba{[]int{1, 3, 5}, 9},
		prueba{[]int{1, 4, 5}, 10},
		prueba{[]int{2, 3, 6}, 11},
		prueba{[]int{1, 4, 7}, 12},
	}

	for _, x := range pruebas {
		v := myAdd(x.datos...)
		if v != x.respuesta {
			t.Error("Expected", 10, "Got", v)
		}
	}
}
