package main

import "fmt"

func main() {
	fmt.Println("La suma de 2 + 4 es :", myAdd(2, 4))
	fmt.Println("La suma de 1 + 5 es :", myAdd(1, 5))
	fmt.Println("La suma de 6 + 7 es :", myAdd(6, 7))

}

func myAdd(xi ...int) int {
	var sum int
	for _, v := range xi {
		sum += v
	}
	return sum
}
