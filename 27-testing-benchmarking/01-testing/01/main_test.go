package main

import "testing"

func TestMyAdd(t *testing.T) {
	v := myAdd(2, 8)
	if v != 10 {
		t.Error("Expected", 10, "Got", myAdd(2, 8))
	}
}
