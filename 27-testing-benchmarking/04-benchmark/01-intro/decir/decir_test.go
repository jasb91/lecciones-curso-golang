package decir

import (
	"fmt"
	"testing"
)

func TestSaludar(t *testing.T) {
	s := Saludar("Jhon")

	if s != "Bienvenido querido Jhon" {
		t.Error("Expected", "Bienvenido querido Jhon", "Got", s)
	}
}

func ExampleSaludar() {
	fmt.Println(Saludar("Jhon"))
	//Output:
	//Bienvenido querido Jhon
}

func BenchmarkSaludar(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Saludar("Jhon")
	}
}
