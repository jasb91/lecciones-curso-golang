package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
	"time"
)

var start = time.Now()

func main() {
	fmt.Println(
		"OS:\t", runtime.GOOS,
		"\nARCH:\t", runtime.GOARCH,
		"\n# CPU's:\t", runtime.NumCPU(),
		"\n# Goroutines:\t", runtime.NumGoroutine(),
	)

	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)
	var contador int64
	for i := 0; i < gs; i++ {
		go func() {
			atomic.AddInt64(&contador, 1)
			runtime.Gosched()
			fmt.Println("Contador:", atomic.LoadInt64(&contador))
			wg.Done()
		}()
		fmt.Println("# Goroutines at end:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("Cuenta:", contador)
	duration := time.Since(start)
	fmt.Println("Duración de la compilación:", duration)

}
