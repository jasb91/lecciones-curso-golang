package main

import (
	"fmt"
	"runtime"
	"time"
)

var start = time.Now()

//var wg sync.WaitGroup

func foo() {
	for i := 1; i <= 10; i++ {
		fmt.Println("foo:", i)
	}
	//wg.Done()
}

func bar() {
	for i := 1; i <= 10; i++ {
		fmt.Println("bar:", i)
	}
}

func main() {
	fmt.Println(
		"OS\t\t", runtime.GOOS,
		"\nARCH:\t\t", runtime.GOARCH,
		"\nNum CPU's:\t", runtime.NumCPU(),
		"\nNum Goroutines:\t", runtime.NumGoroutine(),
	)

	//wg.Add(1)

	/*go*/ foo()
	fmt.Println()
	bar()

	fmt.Println(
		"\nNum Goroutines:\t", runtime.NumGoroutine(),
	)

	//wg.Wait()
	duration := time.Since(start)
	fmt.Println("Duración de la compilación:", duration)
}
