package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var start = time.Now()

func main() {
	fmt.Println(
		"OS:\t", runtime.GOOS,
		"\nARCH:\t", runtime.GOARCH,
		"\n# CPU's:\t", runtime.NumCPU(),
		"\n# Goroutines:\t", runtime.NumGoroutine(),
	)
	var contador int

	const gs = 100
	var wg sync.WaitGroup
	var mx sync.Mutex
	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			mx.Lock()
			v := contador
			v++
			runtime.Gosched()
			contador = v
			mx.Unlock()
			wg.Done()
		}()
		fmt.Println("# Goroutines at end:", runtime.NumGoroutine())
	}
	wg.Wait()
	fmt.Println("Cuenta:", contador)
	duration := time.Since(start)
	fmt.Println("Duración de la compilación:", duration)

}
