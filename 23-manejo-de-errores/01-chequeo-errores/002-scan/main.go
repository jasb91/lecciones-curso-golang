package main

import "fmt"

func main() {
	var resp1, resp2, resp3 string

	fmt.Print("Nombre: ")
	_, err := fmt.Scan(&resp1)
	if err != nil {
		panic(err)
	}

	fmt.Print("Comida favorita: ")
	_, err = fmt.Scan(&resp2)
	if err != nil {
		panic(err)
	}

	fmt.Print("Deporte favorito: ")
	_, err = fmt.Scan(&resp3)
	if err != nil {
		panic(err)
	}

	fmt.Println(resp1, resp2, resp3)

}
