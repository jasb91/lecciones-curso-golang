package main

import (
	"fmt"
	"log"
	"math"
)

type locError struct {
	lat  string
	long string
	err  error
}

func (n locError) Error() string {
	return fmt.Sprintf("Un error de concepto matemático ha ocurrido en: %v %v %v", n.lat, n.long, n.err)
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		errMath := fmt.Errorf("Matemática elemental: no existe raíces cuadradas de números negativos:, %v", f)
		return 0, locError{"50.2289 N", "99.4656 W", errMath}
	}
	return math.Pow(f, 0.5), nil
}

func main() {
	_, err := sqrt(-100)
	if err != nil {
		log.Println(err)
	}
}
