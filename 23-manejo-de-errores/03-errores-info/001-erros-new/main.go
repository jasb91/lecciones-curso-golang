package main

import (
	"errors"
	"fmt"
	"log"
	"math"
)

func main() {
	sqrt, err := sqrt(-100.0)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(sqrt)
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		return 0, errors.New("Matemática elemental: no existe raíces cuadradas de números negativos")
	}
	return math.Pow(f, 0.5), nil
}
