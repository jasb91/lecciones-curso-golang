package main

import (
	"fmt"
	"os"
)

func main() {
	defer foo()
	_, err := os.Open("there-are-no.txt")
	if err != nil {
		panic(err)
	}
}

func foo() {
	fmt.Println("Cuando os.Exit() es llamada, las funciones diferidas no corren.")
}
