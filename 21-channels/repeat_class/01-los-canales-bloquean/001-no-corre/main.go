package main

import "fmt"

func main() {
	// Declaración de variable (canal sin búfer)
	ch := make(chan int)

	ch <- 5
	fmt.Println(<-ch)

}
