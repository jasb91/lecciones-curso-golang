package main

import "fmt"

func main() {
	// Declaración de variable (canal con búfer)
	ch := make(chan int, 1)
	fmt.Println(ch)
	fmt.Printf("%T\n", ch)

	ch <- 5

	fmt.Println(<-ch)

}
