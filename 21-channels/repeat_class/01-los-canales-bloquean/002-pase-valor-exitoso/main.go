package main

import "fmt"

func main() {
	// Declaración de variable (canal sin búfer)
	ch := make(chan int)
	fmt.Println(ch)
	fmt.Printf("%T\n", ch)

	go func() {
		ch <- 5
	}()

	fmt.Println(<-ch)

}
