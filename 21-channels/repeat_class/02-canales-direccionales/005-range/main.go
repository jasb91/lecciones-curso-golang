package main

import "fmt"

func main() {
	ch := make(chan int)

	// Send only
	go func() {
		for i := 0; i < 5; i++ {
			ch <- i
		}
		close(ch)
	}()

	// Reciever only
	func() {
		for v := range ch {
			fmt.Println(v)
		}
	}()

	fmt.Println("Finalizando...")
}
