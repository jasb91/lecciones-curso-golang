package main

import "fmt"

func main() {
	ch := make(chan int)

	// Send only
	go enviar(ch)
	recibir(ch)

	fmt.Println("Finalizando...")
}

func enviar(ch chan<- int) {
	ch <- 5
}

func recibir(ch <-chan int) {
	fmt.Println(<-ch)
}
