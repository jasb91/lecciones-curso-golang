package main

import "fmt"

func main() {
	// Declaración de variable (canal con búfer)
	ch := make(<-chan int, 2)

	ch <- 5
	ch <- 6

	fmt.Println(<-ch)
	fmt.Println(<-ch)
	fmt.Println("--------")
	fmt.Printf("%T\n", ch)

}
