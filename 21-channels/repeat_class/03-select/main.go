package main

import "fmt"

func enviar(p, i, e chan<- int) {
	for j := 0; j < 100; j++ {
		if j%2 == 0 {
			p <- j
		} else {
			i <- j
		}
	}
	// close(p)
	// close(i)
	e <- 0
}

// Recibir

func recibir(p, i, e <-chan int) {
	for {
		select {
		case v := <-p:
			fmt.Println("Desde el canal par:", v)
		case v := <-i:
			fmt.Println("Desde el canal impar:", v)
		case v := <-e:
			fmt.Println("Desde el canal exit:", v)
			return
		}
	}
}

func main() {
	chPar := make(chan int)
	chImpar := make(chan int)
	exit := make(chan int)
	// Enviar
	go enviar(chPar, chImpar, exit)

	recibir(chPar, chImpar, exit)

	fmt.Println("Finalizando...")

}
