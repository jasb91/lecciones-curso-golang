package main

import "fmt"

func main() {
	// Buffer channels (Canales con búfer)
	ca := make(chan int, 1)

	ca <- 42

	fmt.Println(<-ca)

}
