package main

import "fmt"

func main() {
	// Unbuffer channels (Canales sin búfer)
	ca := make(chan int)

	go func() {
		ca <- 42
	}()

	fmt.Println(<-ca)

}
