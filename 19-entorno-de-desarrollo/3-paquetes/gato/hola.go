package gato

import "fmt"

// Hola da un saludo de un gato
func Hola() {
	fmt.Println("Hola desde gato")
}

// Comen lo que comen los gatos
func Comen() {
	fmt.Println("Los gatos comen ratones")
}
